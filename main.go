package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"sync"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", "localhost:5000", "http service address")
var livePlayerAddr = flag.String("live-player-addr", "http://localhost:8081", "http service address")
var loketEventName = flag.String("event-name", "test-onlyfans_lb1", "event name")
var chunkSize = flag.Int("chunk", 100, "Number of attendee per batch")
var newBatch = flag.Int("new-batch-in-s", 5, "Number of attendee per batch")
var beatInterval = flag.Duration("beat-interval", 5, "Send action_type \"beat\" interval")
var eventId = flag.String("event-id", "2373", "event id")
var fileJson = flag.String("file", "attende.json", "json file")
var load = flag.String("load", "ws", "load test ws or player")

type Attendee struct {
	BarcodeId   string `json:"barcode_id"`
	VoucherCode string `json:"voucher_code"`
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	var wg sync.WaitGroup
	shutdown := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	errCounterChannel := make(chan int)
	connectedCounterChannel := make(chan int)
	connectedCounterSum := 0
	errCountSum := 0
	signal.Notify(interrupt, os.Interrupt)
	file, err := ioutil.ReadFile(*fileJson)
	if err != nil {
		log.Fatal("FILE NOT FOUND", err)
	}
	attendees := []Attendee{}
	chunk := *chunkSize
	firstIndex := 0
	json.Unmarshal(file, &attendees)
	n := len(attendees)
	for {
		if firstIndex >= n {
			log.Println(firstIndex > n, "firstIndex IS LARGEER THANN len of attendee")
			break
		}
		chunkedAttendees := attendees[firstIndex:chunk]
		ticker := time.NewTicker(time.Second * time.Duration(*newBatch))
		select {
		case ccnt := <-connectedCounterChannel:
			connectedCounterSum += ccnt
		case cnt := <-errCounterChannel:
			errCountSum += cnt
		case <-interrupt:
			return
		case <-ticker.C:
			log.Println("CONNECTED CLIENT", connectedCounterSum)
			log.Println("errCounter", errCountSum)
			for _, attendee := range chunkedAttendees {
				wg.Add(1)
				token := generateTokenJwt(attendee)
				u := url.URL{Scheme: "wss", Host: *addr, Path: "/live-guard", ForceQuery: true, RawQuery: fmt.Sprintf("token=%v", token)}
				if *load == "ws" {
					go connectClient(u, shutdown, wg, errCounterChannel, connectedCounterChannel)
				} else {

					go loadLivePlayer(attendee, shutdown)
				}
			}

			if firstIndex < n {
				firstIndex += *chunkSize
			}
			if chunk+*chunkSize > n {
				chunk = n
			} else {
				chunk += *chunkSize
			}
		}
	}
	for {
		select {
		case <-interrupt:
			log.Println("SHUTODWON")
			close(shutdown)
			return
		}
	}
	wg.Wait()
}

func generateTokenJwt(attendee Attendee) string {
	sign := jwt.New(jwt.GetSigningMethod("HS256"))
	claims := sign.Claims.(jwt.MapClaims)
	claims["event_id"] = string(*eventId)
	claims["voucher_code"] = attendee.VoucherCode
	claims["barcode_id"] = attendee.BarcodeId
	claims["session"] = fmt.Sprintf("%v%v", attendee.BarcodeId, attendee.VoucherCode)
	secret := "liveguardintegration"
	token, _ := sign.SignedString([]byte(secret))
	return token
}
func connectToLiveEvent(c *websocket.Conn) {
	err := c.WriteJSON(map[string]interface{}{
		"action_type": "connect_to_live_event",
	})
	if err != nil {
		log.Println(err, "EERR")
	}
}
func sendBeat(c *websocket.Conn) {
	err := c.WriteJSON(map[string]interface{}{
		"action_type": "beat",
	})
	if err != nil {
		log.Println(err, "EERR")
	}
}

func loadLivePlayer(attendee Attendee, shutdown chan struct{}) error {
	client := &http.Client{}
	uri := fmt.Sprintf("%v/%v/%v/%v/%v", *livePlayerAddr, "live", *loketEventName, attendee.VoucherCode, attendee.BarcodeId)
	req, err := http.NewRequest("GET", uri, bytes.NewBuffer([]byte("")))
	res, err := client.Do(req)
	defer func() {
		if res == nil {
			log.Println(res)
			return
		}
		if res.Body == nil {
			log.Println(res)
			return
		}
		if err := res.Body.Close(); err != nil {
			log.Println(err)
		}
	}()

	if res == nil {
		log.Println(res)
		return errors.New("NIL res")
	}
	if res.StatusCode == 200 {
		return nil
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
	}
	log.Println(string(body))
	return nil
}
func connectClient(u url.URL, shutdown chan struct{}, wg sync.WaitGroup, errCounter, connectedCounter chan int) {
	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Println("dial:", err)
		errCounter <- 1
		return
	}
	connectToLiveEvent(c)
	connectedCounter <- 1
	defer c.Close()
	done := make(chan struct{})
	go func() {
		defer close(done)
		for {
			_, _, err := c.ReadMessage()
			if err != nil {
				log.Printf("recv: %s", err)
				done <- struct{}{}
			} else {
				// log.Printf("recv: %s", message)
			}
		}
	}()

	ticker := time.NewTicker(time.Second * (*beatInterval))

	defer ticker.Stop()
	for {
		select {
		case <-done:
			log.Println("CLOSE KICKED", u.String())
			return
		case <-ticker.C:
			sendBeat(c)
		case <-shutdown:
			log.Println("CLOSE CON", u.String())
			if err != nil {
				err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
				if err != nil {
					log.Println("write close:", err)
					return
				}
				log.Println("write close:", err)
				return
			}
			return
		}
	}

}
