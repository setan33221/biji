package utils

import (
	"encoding/json"
	"fmt"
	"os"
)

// MapToJSONByte map to json string byte
func MapToJSONByte(m interface{}) []byte {
	enc := json.NewEncoder(os.Stdout)
	err := enc.Encode(m)
	if err != nil {
		fmt.Println(err.Error())
	}

	jsonString, _ := json.Marshal(m)
	return jsonString
}

// StringJSONToMap cconvert string json to map
func StringJSONToMap(s string) interface{} {
	var respMap interface{}
	println(s)
	errJSON := json.Unmarshal([]byte(s), &respMap)
	if errJSON != nil {
		fmt.Println(errJSON.Error())
		return map[string]interface{}{}
	}
	return respMap
}
