package clients

import (
	"fmt"
	"os"
	"time"

	"github.com/go-redis/redis/v7"
)

type RedisClient struct {
	client *redis.Client
}

func NewRedisClient() *RedisClient {
	return &RedisClient{
		client: redis.NewClient(&redis.Options{
			Addr:     fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
			Password: os.Getenv("REDIS_PASSWORD"),
		}),
	}
}

func (rc *RedisClient) Set(key string, value interface{}, expired time.Duration) {
	rc.client.Set(fmt.Sprintf("%s:%s", os.Getenv("REDIS_PREFIX"), key), value, expired)
}

func (rc *RedisClient) Get(key string) string {
	fullkey := fmt.Sprintf("%s:%s", os.Getenv("REDIS_PREFIX"), key)
	return rc.client.Get(fullkey).Val()
}
