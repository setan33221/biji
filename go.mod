module wsliveguard

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/elliotchance/phpserialize v1.2.0
	github.com/go-redis/redis/v7 v7.2.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.5.0
	github.com/stretchr/objx v0.1.1 // indirect
)
